import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';
import { HeroesComponent }      from './heroes/heroes.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';
import { FaqComponent } from './faq/faq.component';
import { CustomerComponent } from './customer/customer.component';
import { GeneralComponent } from './general/general.component';
import { VenderComponent } from './vender/vender.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'detail/:id', component: HeroDetailComponent },
  { path: 'heroes', component: HeroesComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'faq/customer', component: CustomerComponent },
  { path: 'faq/general', component: GeneralComponent },
  { path: 'faq/vendor', component: VenderComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}